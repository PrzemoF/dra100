#!/usr/bin/env python3
# coding: utf-8
"""
Simple GTK application to control DRA-100 over network
"""

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import GObject

import dra100

IP = "192.168.0.100"


class handler:
    def __init__(self):
        self.running = True
        self.change_by_hardware = False
        self.power_btn_initialised = False
        self.waiting_for_hardware = False
        self.waiting_for_state = -1

        self.denon = dra100.dra100(IP)
        self.denon.get_all_info()
        self.refresh_data()

    def refresh_data(self):
        hardware_power_state = self.denon.device_state['power']
        if hardware_power_state is None:
            self.denon.get_power()
        elif not self.waiting_for_hardware:
            # Initialise power button
            if not self.power_btn_initialised:
                self.set_power_sw(hardware_power_state)
                self.change_by_hardware = False

            # Get power button statw
            power_btn_state = self.get_power_sw_state()

            # Button state doesn't match hardware state
            if power_btn_state != hardware_power_state:
                if power_btn_state == 'ON':
                    if self.change_by_hardware:
                        self.set_power_sw('STANDBY')
                        self.denon.power_off()
                    else:
                        self.set_power_sw('ON')
                        self.denon.power_on()
                        self.waiting_for_state = 'ON'
                        self.waiting_for_hardware = True
                if power_btn_state == 'STANDBY':
                    if self.change_by_hardware:
                        self.set_power_sw('ON')
                        self.denon.power_on()
                    else:
                        self.set_power_sw('STANDBY')
                        self.denon.power_off()
                        self.waiting_for_state = 'STANDBY'
                        self.waiting_for_hardware = True
        else:
            if self.waiting_for_state == hardware_power_state:
                self.waiting_for_hardware = False

        if self.running:
            GObject.timeout_add(1000, self.refresh_data)

    def on_power_sw_state_set(self, widget, state):
        self.change_by_hardware = False

    def on_power_sw_notify(self, widget, signal):
        pass

    def get_power_sw_state(self):
        power_sw = builder.get_object('power_sw')
        state = power_sw.get_state()
        if state:
            s = 'ON'
        else:
            s = 'STANDBY'
        return s

    def set_power_sw(self, state):
        if not self.power_btn_initialised:
            self.power_btn_initialised = True
        self.change_by_hardware = True
        power_sw = builder.get_object('power_sw')
        if state == 'ON':
            power_sw.set_active(True)
        else:
            power_sw.set_active(False)

    def on_destroy(self, *args):
        self.running = False
        Gtk.main_quit()
        self.denon.stop()


builder = Gtk.Builder()
builder.add_from_file("ui/power.glade")
win = builder.get_object('main_win')
settings = Gtk.Settings.get_default()
settings.set_property("gtk-application-prefer-dark-theme", True)
builder.connect_signals(handler())
win.show_all()
Gtk.main()
