import ipaddress
import re
import socket
from subprocess import PIPE
from subprocess import Popen

from mac_vendor_lookup import MacLookup


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        ip = s.getsockname()[0]
    except Exception:
        ip = '127.0.0.1'
    finally:
        s.close()
    return ip


#get mac
def get_mac(ip):
    pid = Popen(['arp', '-D', ip], stdout=PIPE, stderr=PIPE)
    out = pid.communicate()

    #get mac from arp
    p = re.compile(r'(?:[0-9a-fA-F]:?){12}')
    #test_str = u"TEXT WITH SOME MAC ADDRESSES 00:24:17:b1:cc:cc TEXT CONTINUES WITH SOME MORE TEXT 20:89:86:9a:86:24"
    try:
        mac = re.findall(p, out[0].decode('utf-8'))[0]
        vendor = MacLookup().lookup(mac)
        print(ip, vendor)
        #"D&M Holdings Inc."
        if  vendor == "D&M Holdings Inc.":
            print("Denon device found")
    except IndexError:
        pass


net4 = ipaddress.ip_network('192.168.1.0/24') # FIXME
for x in net4.hosts():
    get_mac(str(x))
