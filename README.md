# pydra100
UPDATE: Open source solution replacing vtuner service: https://github.com/milaq/YCast
WARNING (14 Feb 2020) internet radio on DRA-100 on longer works as underlying vtuner.com service switched to donation (paid subscription). I'm waiting for Denon customer support to provice API documentation, if possible, to build some open source solution to that problem.

Python3/GTK3 interface for Denon DRA-100

![screenshot](https://raw.githubusercontent.com/PrzemoF/pydra100/master/images/dra100_screenshot.png)

Start test application:
```
python3 controler_dra100.py
```

Comments:
- DRA-100 happily transmit some info as the content of the screen even if it's not true. I.e. When the device is in stand-by mode (empty real display) is still shows some content over telnet. In short: the screen is not reflected in telnet session as expected.
- Sometimes DRA-100 expects user action (like: Send usage data? Yes/No) and there is no way to guess that remotely
- Favourites names are only 32 bytes long, so "MC Radio 102.7 F" instead of "MC Radio 102.7 FM" is normal

Known bugs:
- a lot, but it's usable :-)
