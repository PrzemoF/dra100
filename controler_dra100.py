#!/usr/bin/env python3
# coding: utf-8
"""
Simple GTK application to control DRA-100 over network
"""

import dra100

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib
from gi.repository import Gtk


IP = "192.168.0.100"


class handler:
    def __init__(self):
        self.running = True
        self.active_source = None
        self.display_lines = dict()
        self.change_by_hardware = False
        self.power_btn_initialised = False
        self.waiting_for_hardware = False
        self.waiting_for_state = -1

        self.denon = dra100.dra100(IP)
        self.denon.get_all_info()
        # short link to hardware state
        self.hs = self.denon.device_state

        if self.denon.device_state['name'] is not None:
            win = builder.get_object('main_win')
            win.set_title(self.denon.device_state['name'])

        self.volume_scl = builder.get_object('volume_scl')
        self.volume_scl.set_range(-91, 0)

        self.source_input_ls = builder.get_object('source_input_ls')
        for iname in self.denon.SOURCE:
            self.source_input_ls.append([self.denon.SOURCE[iname], iname])

        source_cbox = builder.get_object('source_cbox')
        cell = Gtk.CellRendererText()
        source_cbox.pack_start(cell, True)
        source_cbox.add_attribute(cell, "text", 0)

        self.denon.get_favourites()

        self.favourites_ls = builder.get_object('favourites_ls')
        for position in self.denon.favourites:
            name = self.denon.favourites[position]
            if name != '':
                self.favourites_ls.append([position, name])

        favourites_cbox = builder.get_object('favourites_cbox')
        cell = Gtk.CellRendererText()
        favourites_cbox.pack_start(cell, True)
        favourites_cbox.add_attribute(cell, "text", 1)
        #active_favourites = self.denon.SOURCE[self.denon.device_state['_favourites']]
        #favourites_cbox.set_active_id(active_favourites)
        GLib.timeout_add_seconds(1, self.refresh_data)

    def refresh_data(self):
        self.handle_power()
        if self.hs['source'] is None:
            self.denon.get_source()
        else:
            self.set_source_cbox(self.hs['source'])

        if self.hs['_volume'] is None:
            self.denon.get_volume()
        else:
            self.set_volume_scl()

        self.denon.get_display()
        for no, content in self.denon.display_lines.items():
            self.display_lines[no] = content

        self.refresh_display()
        self.refresh_audio_data_lbl()
        #print(self.denon.display_lines_state)

        # refresh_data is started by GLib.timeout_add_seconds. Return from the function decides if it should be called again
        # https://developer.gnome.org/pygobject/stable/glib-functions.html#function-glib--timeout-add-seconds
        return self.running

    def refresh_display(self):
        for no, content in self.display_lines.items():
            lbl = builder.get_object('display_line_{}_lbl'.format(no + 1))
            if lbl is not None:
                try:
                    line = self.display_lines[no]
                except KeyError:
                    line = ''
                lbl.set_text(line)

    def on_power_sw_state_set(self, widget, state):
        self.change_by_hardware = False

    def on_source_cbox_changed(self, widget):
        active_source = self.source_input_ls[widget.get_active()][1]
        if self.active_source != active_source:
            self.active_source = active_source
            #self.denon.source_direct(self.active_source)

    def on_favourites_cbox_changed(self, widget):
        position = self.favourites_ls[widget.get_active()][0]
        self.denon.favourites_direct(position)

    def on_play_btn_clicked(self, widget):
        self.denon.control_play()

    def on_pause_btn_clicked(self, widget):
        self.denon.control_pause()

    def on_stop_btn_clicked(self, widget):
        self.denon.control_stop()

    def on_previous_btn_clicked(self, widget):
        self.denon.control_previous()

    def on_next_btn_clicked(self, widget):
        self.denon.control_next()

    def refresh_audio_data_lbl(self):
        adl = builder.get_object('audio_data_lbl')
        if self.denon.device_state['audio_format'] is not None:
            aft = self.denon.device_state['audio_format']
        else:
            aft = ''
        if self.denon.device_state['audio_freq'] is not None:
            afq = self.denon.device_state['audio_freq']
        else:
            afq = ''
        adl.set_label(aft + ' ' + afq)

    def set_source_cbox(self, source):
        source_cbox = builder.get_object('source_cbox')
        source_cbox.set_active_id(source)

    def set_volume_scl(self):
        if self.hs['_volume'] is not None:
            self.volume_scl.set_value(-1 * int(self.hs['_volume']))

    def on_volume_scl_format_value(self, widget, value):
        v = '{:.0f} dB'.format(value)
        return v

    def on_volume_scl_value_changed(self, widget):
        v = widget.get_value()
        vs = '{:.0f}'.format(-1 * int(v))
        self.denon.volume_direct(int(vs))

    def on_mute_btn_clicked(self, widget):
        if self.hs['mute'] == 'OFF':
            self.denon.mute_on()
            self.volume_scl.set_sensitive(False)
        elif self.hs['mute'] == 'ON':
            self.denon.mute_off()
            self.volume_scl.set_sensitive(True)
        else:
            print("Mute is: {}, won't change it".format(self.hs['mute']))

    def on_destroy(self, *args):
        self.running = False
        Gtk.main_quit()
        self.denon.stop()

    def handle_power(self):
        hardware_power_state = self.denon.device_state['power']
        if hardware_power_state is None:
            self.denon.get_power()
        elif not self.waiting_for_hardware:
            # Initialise power button
            if not self.power_btn_initialised:
                self.set_power_sw(hardware_power_state)
                self.change_by_hardware = False

            # Get power button statw
            power_btn_state = self.get_power_sw_state()

            # Button state doesn't match hardware state
            if power_btn_state != hardware_power_state:
                if power_btn_state == 'ON':
                    if self.change_by_hardware:
                        self.set_power_sw('STANDBY')
                        self.denon.power_off()
                    else:
                        self.set_power_sw('ON')
                        self.denon.power_on()
                        self.waiting_for_state = 'ON'
                        self.waiting_for_hardware = True
                if power_btn_state == 'STANDBY':
                    if self.change_by_hardware:
                        self.set_power_sw('ON')
                        self.denon.power_on()
                    else:
                        self.set_power_sw('STANDBY')
                        self.denon.power_off()
                        self.waiting_for_state = 'STANDBY'
                        self.waiting_for_hardware = True
        else:
            if self.waiting_for_state == hardware_power_state:
                self.waiting_for_hardware = False

    def get_power_sw_state(self):
        power_sw = builder.get_object('power_sw')
        state = power_sw.get_state()
        if state:
            s = 'ON'
        else:
            s = 'STANDBY'
        return s

    def set_power_sw(self, state):
        if not self.power_btn_initialised:
            self.power_btn_initialised = True
        self.change_by_hardware = True
        power_sw = builder.get_object('power_sw')
        if state == 'ON':
            power_sw.set_active(True)
        else:
            power_sw.set_active(False)


builder = Gtk.Builder()
builder.add_from_file("ui/dra100.glade")
win = builder.get_object('main_win')
settings = Gtk.Settings.get_default()
settings.set_property("gtk-application-prefer-dark-theme", True)
builder.connect_signals(handler())
win.show_all()
Gtk.main()
