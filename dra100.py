#!/usr/bin/env python3
# coding: utf-8
"""
    Module handling Denon DRA-100 over telnet
"""

import collections
import telnetlib
import threading
import time


class Error(Exception):
    """Custom exception base class"""
    def __init__(self, message, payload=None):
        super().__init__(message)
        self.message = message
        self.payload = payload

    def __str__(self):
        return str(self.message)


class dra100():
    COMMAND = {'power on': b'PWON',
               'power status': b'PW?',
               'power off': b'PWSTANDBY',
               'volume up': b'MVUP',
               'volume status': b'MV?',
               'volume': b'MV',
               'volume down': b'MVDOWN',
               'play': b'NS9A',
               'pause': b'NS9B',
               'stop': b'NS9C',
               'next': b'NS9D',
               'previous': b'NS9E',
               'get source': b'SI?',
               'set source': b'SI',
               'source iradio': b'SIIRADIO',
               'source server': b'SISERVER',
               'source bluetooth': b'SIBLUETOOTH',
               'source usb': b'SIUSB',
               'source coaxial': b'SICOAXIAL',
               'source optical 1': b'SIDIGITALIN1',
               'source optical 2': b'SIDIGITALIN2',
               'source analog 1': b'SIANALOGIN',
               'source analog 2': b'SIANALOGIN2',
               'get favourites': b'FV ?',
               'get format': b'SSFMT?',
               'favourites direct': b'FV ',
               'get info': b'NSINF?',
               'get display': b'NSE',
               'mute on': b'MUON',
               'mute status': b'MU?',
               'mute off': b'MUOFF'}

    SOURCE = {'IRADIO': 'Internet Radio',
              'SERVER': 'Music Server',
              'BLUETOOTH': 'Bluetooth',
              'USB': 'USB',
              'COAXIAL': 'Coaxial',
              'DIGITALIN1': 'Optical 1',
              'DIGITALIN2': 'Optical 2',
              'ANALOGIN': 'Analog In1',
              'ANALOGIN2': 'Analog In2'}

    NETWORK = {'WIR': 'Wired',
               'WIF': 'Wireless'}

    FORMAT = {'LPC': 'LPCM',
              'MP3': 'MP3',
              'WMA': 'WMA',
              'AAC': 'AAC',
              'FLC': 'FLAC',
              'ALC': 'ALAC',
              'AIF': 'AIFF',
              'DSD': 'DSD',
              'ULC': 'Signal Unlock',
              'UPS': 'Unsupported'}

    FREQEUNCY = {'032': '32 kHz',
                 '044': '44.1 kHz',
                 '048': '48 kHz',
                 '088': '88.2 kHz',
                 '096': '96 kHz',
                 '176': '176.4 kHz',
                 '192': '192 kHz'}

    def __init__(self, ip=None):
        self.device_state = dict(ip=ip,
                                 name=None,
                                 _network=None,
                                 network=None,
                                 dhcp=None,
                                 power=None,
                                 _source=None,
                                 source=None,
                                 _volume=None,
                                 volume=None,
                                 _audio_format=None,
                                 audio_format=None,
                                 _audio_freq=None,
                                 audio_freq=None,
                                 mute=None)
        self.display_lines = dict()
        self.display_lines_state = dict()
        # FIXME Add modified display lines showing "real" display content (i.e. active source)
        self.favourites = dict()
        self._favourites_source_type = dict()
        self.commands = collections.deque()
        self.transmitter_thread = threading.Thread(target=self.transmitter).start()

    def set_ip(self, ip):
        self.device_state['ip'] = ip

    def _start_session(self):
        try:
            self.device = telnetlib.Telnet(self.device_state['ip'])
        except OSError as e:
            self.device = None
            print(e)

    def send_raw_command(self, command):
        try:
            self.device.write(command)
            self.software_state.append(command)
        except AttributeError:
            pass

    def queue_command(self, command, parameter=None):
        command_used = False
        if len(self.commands) > 0:
            for c in self.commands:
                if c[0] == command:
                    # Command already in the queue, replace parameter value
                    c[1] = parameter
                    command_used = True
        if not command_used:
            self.commands.append([command, parameter])
        #print('{}, {}. {}'.format(command, parameter, self.commands))

    def send_next_command(self):
        if len(self.commands) > 0:
            c = self.commands.popleft()
            cmd = self.COMMAND[c[0]]
            if c[1] is not None:
                parameter = c[1]
                _parameter = format(parameter)
                parameter = bytes(_parameter, 'utf-8')
            else:
                parameter = b''
            self.send_raw_command(cmd + parameter + b'\r')

    def _end_session(self):
        try:
            self.device.close()
        except (EOFError, TypeError, AttributeError) as e:
            pass

    def get_all_info(self):
        self.get_info()
        self.get_power()
        self.get_volume()
        self.get_mute()
        self.get_source()
        self.get_favourites()
        self.get_format()
        time.sleep(0.5)
        return self.device_state

    def get_info(self):
        self.queue_command('get info')

    def get_power(self):
        self.queue_command('power status')

    def power_on(self):
        self.queue_command('power on')
        # Spec says "send a command within 1s of powering on". To be confirmed if it's required
        time.sleep(0.5)
        self.queue_command('get info')

    def power_off(self):
        self.queue_command('power off')
        # Give hardware time to power off
        time.sleep(5)

    def get_volume(self):
        self.queue_command('volume status')

    def get_mute(self):
        self.queue_command('mute status')

    def control_play(self):
        self.queue_command('play')

    def control_pause(self):
        self.queue_command('pause')

    def control_stop(self):
        self.queue_command('stop')

    def control_next(self):
        self.queue_command('stop')

    def control_previous(self):
        self.queue_command('stop')

    def volume_up(self):
        self.queue_command('volume up')

    def volume_direct(self, value):
        self.queue_command('volume', value)

    def volume_down(self):
        self.queue_command('volume down')

    def mute_on(self):
        self.queue_command('mute on')

    def mute_off(self):
        self.queue_command('mute off')

    def source_iradio(self):
        self.queue_command('source iradio')

    def source_server(self):
        self.queue_command('source server')

    def source_bluetooth(self):
        self.queue_command('source bluetooth')

    def source_usb(self):
        self.queue_command('source usb')

    def source_coaxial(self):
        self.queue_command('source coaxial')

    def source_optical_1(self):
        self.queue_command('source optical 1')

    def source_optical_2(self):
        self.queue_command('source optical 2')

    def source_analog_1(self):
        self.queue_command('source analog 1')

    def source_analog_2(self):
        self.queue_command('source analog 2')

    def source_direct(self, source):
        self.queue_command('set source', source)

    def get_source(self):
        self.queue_command('get source')

    def get_display(self):
        self.queue_command('get display')

    def get_favourites(self):
        self.queue_command('get favourites')
        time.sleep(1.0)

    def get_format(self):
        self.queue_command('get format')

    def favourites_direct(self, position):
        self.queue_command('favourites direct', position)

    def parse_response(self, report):
        field = report.decode().strip()
        if field.startswith('PW'):
            self.device_state['power'] = field[2:]
        elif field.startswith('SI'):
            self.device_state['_source'] = field[2:]
            self.device_state['source'] = self.SOURCE[field[2:]]
        elif field.startswith('MV'):
            self.device_state['_volume'] = field[2:]
            self.set_volume_string(field[2:])
        elif field.startswith('MU'):
            self.device_state['mute'] = field[2:]
        elif field.startswith('NS'):
            if field.startswith('NSINFF'):
                self.device_state['name'] = field[6:]
            elif field.startswith('NSINFAFF '):
                self.device_state['_network'] = field[9:]
                self.device_state['network'] = self.NETWORK[field[9:]]
            elif field.startswith('NSINNSINFDHC '):
                self.device_state['dhcp'] = field[13:]
            elif field.startswith('NSINMV '):
                self.device_state['_volume'] = field[6:]
                self.set_volume_string(field[6:])
            elif field.startswith('NSE'):
                no = int(field[3:4])
                self.display_lines_state[no] = ''
                # That doesn't match the denond spec. Line 7 has the extra byte?
                self.display_lines_state[no] = '-'
                if no in [1, 2, 3, 4, 5, 6, 7]:
                    start = 5
                    f = field[4:5]
                    if f != '':
                        self.display_lines_state[no] = ord(f) & 0b00001001
                    if self.display_lines_state[no] == 0:
                        self.display_lines_state[no] = '-'
                else:
                    start = 4
                self.display_lines[no] = field[start:].strip()
        elif field.startswith('FV'):
            no = field[2:4]
            self.favourites[no] = field[8:].strip()
            self._favourites_source_type[no] = field[5:7]
        elif field.startswith('SS'):
            if field.startswith('SSFMT'):
                self.device_state['_audio_format'] = field[5:8]
                try:
                    self.device_state['audio_format'] = self.FORMAT[field[5:8]]
                except KeyError:
                    self.device_state['audio_format'] = field[5:8]
                try:
                    self.device_state['audio_freq'] = self.FREQEUNCY[field[8:]]
                except KeyError:
                    self.device_state['audio_freq'] = field[8:]
                #print(self.device_state)

    def set_volume_string(self, field):
        if field != '91':
            self.device_state['volume'] = "- {} dB".format(field)
        elif field == '91':
            self.device_state['volume'] = "-∞ dB"

    def transmitter(self):
        self.running = True
        self._start_session()
        while self.running:
            try:
                ret = self.device.read_until(b'\r', 0.25)
                #[Errno 111] Połączenie odrzucone
                #Device is None, connection problems
                #print(ret)
                self.parse_response(ret)
            except EOFError as e:
                print("Connection problems")
                print(e)
                time.sleep(1)
            except AttributeError as e:
                print("Device is None, connection problems")
                print(e)
                time.sleep(1)
            except TypeError as e:
                print("Device doesn't exits, connection problems")
                print(e)
                time.sleep(1)
            self.send_next_command()
        self._end_session()

    def stop(self):
        self.running = False

    def __del__(self):
        self.stop()
